package Writer;

import java.io.IOException;
import java.io.PrintWriter;
import java.nio.charset.StandardCharsets;
import java.util.Arrays;
import java.util.Scanner;

public class MyOutputOrFileWriter {
    /**
     * Для методов аппроксимации линейной, логарифмической, экспоненциальной и степенной - используются a0 = a, a1 = b
     * Для метода аппроксимации квадратической используются a0, a1, a2;
     * Для метода аппроксимации кубической используются a0, a1, a2, a3;
     */
    final Scanner scanner;
    final int caseData;
    private final double[] coef2;
    private final double[] coef3;
    private final double[] coef4;
    private final double deviationMeasure;
    private final double standartDevision;
    private final int betterDecision;


    public MyOutputOrFileWriter(Scanner scanner, int caseData, double[] coef2, double[] coef3, double[] coef4, double deviationMeasure, double standartDevision, int betterDecision) {
        this.scanner = scanner;
        this.caseData = caseData;
        this.coef2 = coef2;
        this.coef3 = coef3;
        this.coef4 = coef4;
        this.deviationMeasure = deviationMeasure;
        this.standartDevision = standartDevision;
        this.betterDecision = betterDecision;
    }

    private String checkDevasion(int betterDecision) {
        String s;
        switch (betterDecision){
            case 0 -> s = "Логарифмическая аппроксимация";
            case 1 -> s = "Линейная аппроксимация";
            case 2 -> s = "Экспоненциальная аппроксимация";
            case 3 -> s = "Степенная аппроксимация";
            case 4 -> s = "Квадратическая аппроксимация";
            case 5 -> s = "Кубическая аппроксимация";
            default -> s = "Неведомая субстанция";
        }
        return s;
    }

    public void printData() {
        switch (caseData) {
            case 1 -> {
                System.out.println(checkDevasion(betterDecision) + " дала наилучшее приближение. Вот коэффциенты слева направо: ");
                switch(betterDecision) {
                    case 0, 1, 2, 3 -> {
                        System.out.println(Arrays.toString(coef2));
                    }
                    case 4 -> {
                        System.out.println(Arrays.toString(coef3));;
                    }
                    case 5 -> {
                        System.out.println(Arrays.toString(coef4));;
                    }
                    default -> {
                        System.out.println(Arrays.toString(coef2));;
                    }
                }
                System.out.println("Мера отклонения равна: " + deviationMeasure);
                System.out.println("Среднеквадратическое отклонение равно: " + standartDevision);
            }
            case 2 -> {
                try {
                    PrintWriter writer = new PrintWriter("C:\\Users\\MG\\IdeaProjects\\aproximation-function\\Resourses\\output.txt", StandardCharsets.UTF_8);
                    writer.write(checkDevasion(betterDecision) + " дала наилучшее приближение. Вот коэффициенты слева направо: \n");
                    switch(betterDecision) {
                        case 0, 1, 2, 3 -> {
                            writer.write(Arrays.toString(coef2) + "\n");
                        }
                        case 4 -> {
                            writer.write(Arrays.toString(coef3) + "\n");;
                        }
                        case 5 -> {
                            writer.write(Arrays.toString(coef4) + "\n");
                        }
                        default -> {
                            writer.write(Arrays.toString(coef2) + "\n");
                        }
                    }
                    writer.write("Мера отклонения равна: " + deviationMeasure + "\n");
                    writer.write("Среднеквадратическое отклонение равно: " + standartDevision + "\n");
                    writer.close();

                } catch (IOException e) {
                    System.out.println(e.getMessage());
                }
            }
        }
    }
}
