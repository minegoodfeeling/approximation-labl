import InformationPrinter.MainInformationPrinter;
import Methods.*;
import Reader.CaseReader;
import Reader.DataReader;
import Reader.MyInputOrFileReader;
import Writer.MyOutputOrFileWriter;
import org.jfree.data.xy.XYSeries;

import java.util.Arrays;
import java.util.Scanner;

public class Main {

    public static void main(String[] args) {
        while (true) {

            MainInformationPrinter informationPrinter = new MainInformationPrinter();
            Scanner scanner = new Scanner(System.in);

            System.out.println(informationPrinter.typeOfInput());
            CaseReader typeOfReader = new CaseReader(scanner, 1, 2);
            int caseData;
            try {
                caseData = typeOfReader.getCase();
            } catch (Exception e) {
                System.out.println(e.getMessage());
                continue;
            }

            double[] x;
            double[] y;
            if (caseData == 1) System.out.println(informationPrinter.howToInputData());

            MyInputOrFileReader dataReader = new MyInputOrFileReader(scanner, caseData);
            try {
                dataReader.initData();
                x = dataReader.getX();
                y = dataReader.getY();
            } catch (Exception e) {
                System.out.println(e.getMessage());
                continue;
            }

            Method linearApproximation = new LinearApproximation(x, y);
            Method logarithmicApproximation = new LogarithmicFunctionApproximation(x, y);
            Method powerApproximation = new PowerFunctionApproximation(x, y);
            Method exponentialApproximation = new ExponentialFunctionApproximation(x, y);
            Method quadraticApproximation = new QuadraticApproximation(x, y);
            Method cubicalApproximation = new CubicalApproximation(x, y);

            linearApproximation.solve();
            logarithmicApproximation.solve();
            powerApproximation.solve();
            exponentialApproximation.solve();
            quadraticApproximation.solve();
            cubicalApproximation.solve();

            double pearsonCoef = -1;
            try {
                pearsonCoef = linearApproximation.getPearsonCoef();
            } catch (Exception e) {
                e.printStackTrace();
            }

            double[] standartDevasion = new double[6];
            standartDevasion[0] = logarithmicApproximation.getStandartDevasion();
            standartDevasion[1] = linearApproximation.getStandartDevasion();
            standartDevasion[2] = exponentialApproximation.getStandartDevasion();
            standartDevasion[3] = powerApproximation.getStandartDevasion();
            standartDevasion[4] = quadraticApproximation.getStandartDevasion();
            standartDevasion[5] = cubicalApproximation.getStandartDevasion();

            double[] deviantMeasure = new double[6];
            deviantMeasure[0] = logarithmicApproximation.getDeviationMeasure();
            deviantMeasure[1] = linearApproximation.getDeviationMeasure();
            deviantMeasure[2] = exponentialApproximation.getDeviationMeasure();
            deviantMeasure[3] = powerApproximation.getDeviationMeasure();
            deviantMeasure[4] = quadraticApproximation.getDeviationMeasure();
            deviantMeasure[5] = cubicalApproximation.getDeviationMeasure();

            System.out.println("Коэффициенты для log: " + Arrays.toString(logarithmicApproximation.getRoots()));
            System.out.println("Коэффициенты для exp: " + Arrays.toString(exponentialApproximation.getRoots()));
            System.out.println("Коэффициенты для pow: " + Arrays.toString(powerApproximation.getRoots()));
            System.out.println("Коэффициенты для lin: " + Arrays.toString(linearApproximation.getRoots()));
            System.out.println("Коэффициенты для quad: " + Arrays.toString(quadraticApproximation.getRoots()));
            System.out.println("Коэффициенты для cubic: " + Arrays.toString(cubicalApproximation.getRoots()));

            System.out.println("Мера отклонения для lin: " + deviantMeasure[1]);
            System.out.println("Мера отклонения для quad: " + deviantMeasure[4]);

            System.out.println("Среднее отклонение для lin: " + standartDevasion[1]);
            System.out.println("Среднее отклонение для quad: " + standartDevasion[4]);

            System.out.println("Мера отклонения для pow: " + deviantMeasure[3]);
            System.out.println("Среднее отклонение для pow: " + standartDevasion[3]);


            linearApproximation.showGraph();
            logarithmicApproximation.showGraph();
            quadraticApproximation.showGraph();
            cubicalApproximation.showGraph();
            powerApproximation.showGraph();
            exponentialApproximation.showGraph();


            double MIN = Double.MAX_VALUE;
            double EPS = 0.01;
            int betterDecision = 0;
            for (int i = 0; i < 6; i++) {
                if (standartDevasion[i] < MIN + EPS) {
                    MIN = standartDevasion[i];
                    betterDecision = i;
                }
            }
            double[] coef2 = new double[2];
            double[] coef3 = new double[3];
            double[] coef4 = new double[4];

            switch (betterDecision) {
                case 0 -> coef2 = logarithmicApproximation.getRoots();
                case 1 -> coef2 = linearApproximation.getRoots();
                case 2 -> coef2 = exponentialApproximation.getRoots();
                case 3 -> coef2 = powerApproximation.getRoots();
                case 4 -> coef3 = quadraticApproximation.getRoots();
                case 5 -> coef4 = cubicalApproximation.getRoots();
            }
            if (pearsonCoef != -1) System.out.println("Коэффициент Пирсона равен: " + pearsonCoef);

            System.out.println(informationPrinter.howToOutputData());
            CaseReader typeOfWriter = new CaseReader(scanner, 1, 2);
            int caseWriter = 1;
            try {
                caseWriter = typeOfWriter.getCase();
            } catch (Exception e) {
                e.printStackTrace();
            }

            MyOutputOrFileWriter fileWriter = new MyOutputOrFileWriter(scanner, caseWriter, coef2, coef3, coef4,
                    deviantMeasure[betterDecision], standartDevasion[betterDecision], betterDecision);
            fileWriter.printData();
        }
    }
}
