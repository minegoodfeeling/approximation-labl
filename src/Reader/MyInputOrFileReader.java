package Reader;

import java.io.File;
import java.util.Scanner;

public class MyInputOrFileReader{

    final Scanner scanner;
    final int caseData;
    public double[] x;
    public double[] y;



    public MyInputOrFileReader(Scanner scanner, int caseData) {
        this.scanner = scanner;
        this.caseData = caseData;
    }

    public void initData() throws Exception {
        double[] data = null;
        switch (caseData) {
            case 1 -> {
                int numberOfPoint = 0;
                if (scanner.hasNextInt()) numberOfPoint = scanner.nextInt();

                x = new double[numberOfPoint];
                y = new double[numberOfPoint];

                for (int i = 0; i < numberOfPoint; i++) {
                    if (scanner.hasNextLine()) {
                        x[i] = scanner.nextDouble();
                        y[i] = scanner.nextDouble();
                    }
                }
            }
            case 2 -> {
                scanner.nextLine();
                System.out.println("Введите имя файла");
                String fileName = scanner.nextLine();
                File file = new File("C:\\Users\\MG\\IdeaProjects\\aproximation-function\\Resourses\\" + fileName);
                Scanner fileScanner = new Scanner(file);

                int numberOfPoint = 0;
                if (fileScanner.hasNextInt()) numberOfPoint = fileScanner.nextInt();

                x = new double[numberOfPoint];
                y = new double[numberOfPoint];

                for (int i = 0; i < numberOfPoint; i++) {
                    if (fileScanner.hasNextLine()) {
                        x[i] = fileScanner.nextDouble();
                        y[i] = fileScanner.nextDouble();
                    }
                }
            }
            default -> throw new Exception("Упс! Ошибка с вводом данных");
        }
    }

    public double[] getX(){
        return x;
    }

    public double[] getY() {
        return y;
    }
}

//        int numberOfPoint = Integer.parseInt(bufferedReader.readLine());
//        double[] x = new double[numberOfPoint];
//        double[] y = new double[numberOfPoint];
//
//        for (int i = 0; i < numberOfPoint; i++) {
//            String xAndY = bufferedReader.readLine();
//            String[] rowXAndY = xAndY.split(" ");
//
//            x[i] = Double.parseDouble(rowXAndY[0]);
//            y[i] = Double.parseDouble(rowXAndY[1]);
//        }
//
//        double[] result = new double[numberOfPoint * 2];
//
//        for (int i = 0; i < numberOfPoint * 2; i += 2) {
//            result[i] = x[i];
//            result[i + 1] = y[i];
//        }
