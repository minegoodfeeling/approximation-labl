package Reader;

public interface DataReader {
    double[] getData() throws Exception;
}
