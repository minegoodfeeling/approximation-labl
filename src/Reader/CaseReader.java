package Reader;

import java.io.IOException;
import java.util.Scanner;

public class CaseReader {

    final Scanner scanner;
    private final int firstCase;
    private final int lastCase;

    public CaseReader(Scanner scanner, int firstCase, int lastCase) {
        this.scanner = scanner;
        this.firstCase = firstCase;
        this.lastCase = lastCase;
    }

    public int getCase() throws Exception {
        int result = 1;
        if (scanner.hasNextInt()) {
            result = scanner.nextInt();
        } else throw new Exception("Вы должны были ввести натуральное число!");
        if (result < firstCase || result > lastCase) throw new IOException("Вы должны были ввести число от " +
                firstCase + " до " + lastCase);
        return result;
    }
}

