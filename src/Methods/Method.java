package Methods;

public interface Method {
    void solve();
    double[] getRoots();
    double getDeviationMeasure();
    double getStandartDevasion();
    double getPearsonCoef() throws Exception;
    void showGraph();
}
