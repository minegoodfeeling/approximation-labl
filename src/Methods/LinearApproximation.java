package Methods;

import org.jfree.chart.ChartFactory;
import org.jfree.chart.ChartPanel;
import org.jfree.chart.JFreeChart;
import org.jfree.chart.plot.PlotOrientation;
import org.jfree.data.xy.XYDataset;
import org.jfree.data.xy.XYSeries;
import org.jfree.data.xy.XYSeriesCollection;

import javax.sound.sampled.Line;
import javax.swing.*;

public class LinearApproximation implements Method {

    /**
     * Данный метод старается приблизить наши точки линейной функцией
     * Метод getRoots() возвращает массив из 2-х элементов:
     * Первый - a - угловой коэффициент
     * Второй - b - свободный член
     * Сначала мы запускаем метод solve(), после чего можем получить параметры a,b с помощью команды getRoots()
     * а также значение отклонения функции с помощью команды getDeviationMeasure()
     */

    public double[] x;                                  // значения координаты x для заданных точек
    public double[] y;                                  // значения координаты y для заданных точек
    private double a;                                   // угловой коэффициент нашей линейной апроксимации
    private double b;                                   // свободный член нашей линейной апроксимации

    public LinearApproximation(double[] x, double[] y) {
        this.x = x;
        this.y = y;
    }

    private double sumX() {
        int numberOfPoint = x.length;
        double SX = 0;

        for (int i = 0; i < numberOfPoint; i++) {
            SX += x[i];
        }
        return SX;
    }

    private double sumXX() {
        int numberOfPoint = x.length;
        double SXX = 0;

        for (int i = 0; i < numberOfPoint; i++) {
            SXX += x[i] * x[i];
        }
        return SXX;
    }

    private double sumY() {
        int numberOfPoint = y.length;
        double SY = 0;

        for (int i = 0; i < numberOfPoint; i++) {
            SY += y[i];
        }
        return SY;
    }

    private double sumXY() {
        int numberOfPoint = y.length;
        double SXY = 0;

        for (int i = 0; i < numberOfPoint; i++) {
            SXY += y[i] * x[i];
        }
        return SXY;
    }


    @Override
    public void solve() {
        int numberOfPoint = x.length;
        double SXX = sumXX(), SX = sumX(), SY = sumY(), SXY = sumXY();

        double delta = SXX * numberOfPoint - SX * SX;
        double delta1 = SXY * numberOfPoint - SX * SY;
        double delta2 = SXX * SY - SX * SXY;

        a = delta1 / delta;
        b = delta2 / delta;
    }

    @Override
    public double[] getRoots() {
        double[] roots = new double[2];
        roots[0] = a;
        roots[1] = b;

        return roots;
    }

    @Override
    public double getDeviationMeasure() {
        double result = 0;
        double numberOfPoint = x.length;
        for (int i = 0; i < numberOfPoint; i++) {
            double EPS = f(x[i]) - y[i];
            result += EPS * EPS;
        }
        return result;
    }

    @Override
    public double getStandartDevasion() {
        return Math.sqrt(getDeviationMeasure() / x.length);
    }

    public double getPearsonCoef() throws Exception {
        double meanX = meanX();
        double meanY = meanY();
        int numberOfPoint = x.length;
        double numerator = 0;
        double denominatorSumX = 0;
        double denominatorSumY = 0;

        for (int i = 0; i < numberOfPoint; i++) {
            numerator += (x[i] - meanX) * (y[i] - meanY);
            denominatorSumX += (x[i] - meanX) * (x[i] - meanX);
            denominatorSumY += (y[i] - meanY) * (y[i] - meanY);
        }
        if (denominatorSumX * denominatorSumY == 0) throw new Exception("У вас какой-то не такой график!");
        return numerator / Math.sqrt(denominatorSumX * denominatorSumY);
    }

    public void showGraph() {
        XYSeries series = new XYSeries("Linear graph");
        double x0 = this.minX();
        double xn = this.maxX();

        for(float i = (float) ((float) x0 - 0.01); i < xn + 0.01; i+=(xn - x0)/100000) {
            series.add(i, f(i));
        }

        XYDataset xyDataset = new XYSeriesCollection(series);
        JFreeChart chart = ChartFactory
                .createXYLineChart("y = ax+b", "x", "y",
                        xyDataset,
                        PlotOrientation.VERTICAL,
                        true, true, true);
        JFrame frame =
                new JFrame("Linear graph");
        // Помещаем график на фрейм
        frame.getContentPane()
                .add(new ChartPanel(chart));

        frame.setSize(800,800);
        frame.show();
    }

    private double meanX() {
        return sumX() / x.length;
    }

    private double meanY() {
        return sumY() / x.length;
    }

    private double f(double x) {
        return a * x + b;
    }

    private double minX() {
        double min = Double.MAX_VALUE;
        for(int i = 0; i < x.length; i++) {
            if(x[i] < min) min = x[i];
        }
        return min;
    }

    private double maxX() {
        double max = Double.MIN_VALUE;
        for(int i = 0; i < x.length; i++) {
            if(x[i] > max) max = x[i];
        }
        return max;
    }
}