package Methods;

import org.jfree.chart.ChartFactory;
import org.jfree.chart.ChartPanel;
import org.jfree.chart.JFreeChart;
import org.jfree.chart.plot.PlotOrientation;
import org.jfree.data.xy.XYDataset;
import org.jfree.data.xy.XYSeries;
import org.jfree.data.xy.XYSeriesCollection;

import javax.swing.*;

public class LogarithmicFunctionApproximation implements Method{
    public double[] x;                                  // значения координаты x для заданных точек
    public double[] y;                                  // значения координаты y для заданных точек
    private double A;                                   // угловой коэффициент нашей линейной апроксимации
    private double B;                                   // свободный член нашей линейной апроксимации
    private double a;
    private double b;

    public LogarithmicFunctionApproximation(double[] x, double[] y) {
        this.x = x;
        this.y = y;
    }

    private double sumX() {
        int numberOfPoint = x.length;
        double SX = 0;

        for (int i = 0; i < numberOfPoint; i++) {
            SX += X(x[i]);
        }
        return SX;
    }

    private double sumXX() {
        int numberOfPoint = x.length;
        double SXX = 0;

        for (int i = 0; i < numberOfPoint; i++) {
            SXX += X(x[i]) * X(x[i]);
        }
        return SXX;
    }

    private double sumY() {
        int numberOfPoint = y.length;
        double SY = 0;

        for (int i = 0; i < numberOfPoint; i++) {
            SY += y[i];
        }
        return SY;
    }

    private double sumXY() {
        int numberOfPoint = y.length;
        double SXY = 0;

        for (int i = 0; i < numberOfPoint; i++) {
            SXY += y[i] * X(x[i]);
        }
        return SXY;
    }

    @Override
    public void solve() {
        int numberOfPoint = x.length;
        double SXX = sumXX(), SX = sumX(), SY = sumY(), SXY = sumXY();

        double delta = SXX * numberOfPoint - SX * SX;
        double delta1 = SXY * numberOfPoint - SX * SY;
        double delta2 = SXX * SY - SX * SXY;

        A = delta1 / delta;
        B = delta2 / delta;

        a = Math.exp(A);
        b = B;
    }

    @Override
    public double[] getRoots() {
        double[] roots = new double[2];
        roots[0] = a;
        roots[1] = b;

        return roots;
    }

    @Override
    public double getDeviationMeasure() {
        double result = 0;
        double numberOfPoint = x.length;
        for (int i = 0; i < numberOfPoint; i++) {
            double EPS = f(x[i]) - y[i];
            result += EPS * EPS;
        }
        return result;
    }

    @Override
    public double getStandartDevasion() {
        return Math.sqrt(getDeviationMeasure() / x.length);
    }

    public double getPearsonCoef() throws Exception {
        return -1;
    }

    private double minX() {
        double min = Double.MAX_VALUE;
        for(int i = 0; i < x.length; i++) {
            if(x[i] < min) min = x[i];
        }
        return min;
    }

    private double maxX() {
        double max = Double.MIN_VALUE;
        for(int i = 0; i < x.length; i++) {
            if(x[i] > max) max = x[i];
        }
        return max;
    }

    @Override
    public void showGraph() {
        XYSeries series = new XYSeries("Logarithmic graph");
        double x0 = this.minX();
        double xn = this.maxX();

        for(float i = (float) ((float) x0 - 0.01); i < xn + 0.01; i+=(xn - x0)/100000){
            series.add(i, f(i));
            //series.add(i, i);
        }

        XYDataset xyDataset = new XYSeriesCollection(series);
        JFreeChart chart = ChartFactory
                .createXYLineChart("y = a * ln(x) + b", "x", "y",
                        xyDataset,
                        PlotOrientation.VERTICAL,
                        true, true, true);
        JFrame frame =
                new JFrame("Logarithmic graph");
        // Помещаем график на фрейм
        frame.getContentPane()
                .add(new ChartPanel(chart));

        frame.setSize(800,800);
        frame.show();
    }

    private double f(double x) {
        return a * Math.log(x) + b;
    }

    private double X(double x) {
        return Math.log(x);
    }
}
