package Methods;

import LinearEquationSolver.Gaus;
import org.jfree.chart.ChartFactory;
import org.jfree.chart.ChartPanel;
import org.jfree.chart.JFreeChart;
import org.jfree.chart.plot.PlotOrientation;
import org.jfree.data.xy.XYDataset;
import org.jfree.data.xy.XYSeries;
import org.jfree.data.xy.XYSeriesCollection;

import javax.swing.*;

public class QuadraticApproximation implements Method {

    /**
     * метод getRoots() возвращает массив из 3-х элементов, где
     * result[0] = a0;
     * result[1] = a1;
     * result[2] = a2;
     */

    public double[] x;                                  // значения координаты x для заданных точек
    public double[] y;                                  // значения координаты y для заданных точек
    private double a0;                                  // свободный член нашей квадратичной апроксимации
    private double a1;                                  // коэффициент перед x
    private double a2;                                  // коэффициент перед x^2

    public QuadraticApproximation(double[] x, double[] y) {
        this.x = x;
        this.y = y;
    }


    @Override
    public void solve() {
        double[][] A = {{x.length, sumX(), sumXX()},
                {sumX(), sumXX(), sumXXX()},
                {sumXX(), sumXXX(), sumXXXX()}};
        double[] b = {sumY(), sumXY(), sumXXY()};
        Gaus gaus = new Gaus(A, b, 3);
        gaus.makeTriangularMatrix();
        gaus.solve();

        double[] result = gaus.getSolution();
        a0 = result[0];
        a1 = result[1];
        a2 = result[2];
    }

    @Override
    public double[] getRoots() {
        double[] result = new double[3];
        result[0] = a0;
        result[1] = a1;
        result[2] = a2;

        return result;
    }

    @Override
    public double getDeviationMeasure() {
        double result = 0;
        double numberOfPoint = x.length;
        for (int i = 0; i < numberOfPoint; i++) {
            double EPS = f(x[i]) - y[i];
            result += EPS * EPS;
        }
        return result;
    }

    @Override
    public double getStandartDevasion() {
        return Math.sqrt(getDeviationMeasure() / x.length);
    }

    @Override
    public double getPearsonCoef() throws Exception {
        return -1;
    }

    @Override
    public void showGraph() {
        XYSeries series = new XYSeries("Quadratic graph");
        double x0 = this.minX();
        double xn = this.maxX();

        for(float i = (float) ((float) x0 - 0.01); i < xn + 0.01; i+=(xn - x0)/100000){
            series.add(i, f(i));
            //series.add(i, i);
        }

        XYDataset xyDataset = new XYSeriesCollection(series);
        JFreeChart chart = ChartFactory
                .createXYLineChart("y = a0 + a1 * x + a2 * x^2", "x", "y",
                        xyDataset,
                        PlotOrientation.VERTICAL,
                        true, true, true);
        JFrame frame =
                new JFrame("Quadratic graph");
        // Помещаем график на фрейм
        frame.getContentPane()
                .add(new ChartPanel(chart));

        frame.setSize(800,800);
        frame.show();
    }

    private double minX() {
        double min = Double.MAX_VALUE;
        for(int i = 0; i < x.length; i++) {
            if(x[i] < min) min = x[i];
        }
        return min;
    }

    private double maxX() {
        double max = Double.MIN_VALUE;
        for(int i = 0; i < x.length; i++) {
            if(x[i] > max) max = x[i];
        }
        return max;
    }

    private double f(double x) {
        return a0 + a1 * x + a2 * x * x;
    }

    private double sumX() {
        int numberOfPoint = x.length;
        double SX = 0;

        for (int i = 0; i < numberOfPoint; i++) {
            SX += x[i];
        }
        return SX;
    }

    private double sumXX() {
        int numberOfPoint = x.length;
        double SXX = 0;

        for (int i = 0; i < numberOfPoint; i++) {
            SXX += x[i] * x[i];
        }
        return SXX;
    }

    private double sumY() {
        int numberOfPoint = y.length;
        double SY = 0;

        for (int i = 0; i < numberOfPoint; i++) {
            SY += y[i];
        }
        return SY;
    }

    private double sumXY() {
        int numberOfPoint = y.length;
        double SXY = 0;

        for (int i = 0; i < numberOfPoint; i++) {
            SXY += y[i] * x[i];
        }
        return SXY;
    }

    private double sumXXX() {
        int numberOfPoint = x.length;
        double SXXX = 0;

        for (int i = 0; i < numberOfPoint; i++) {
            SXXX += x[i] * x[i] * x[i];
        }
        return SXXX;
    }

    private double sumXXXX() {
        int numberOfPoint = x.length;
        double SXXXX = 0;

        for (int i = 0; i < numberOfPoint; i++) {
            SXXXX += x[i] * x[i] * x[i] * x[i];
        }
        return SXXXX;
    }

    private double sumXXY() {
        int numberOfPoint = y.length;
        double SXXY = 0;

        for (int i = 0; i < numberOfPoint; i++) {
            SXXY += y[i] * x[i] * x[i];
        }
        return SXXY;
    }
}
