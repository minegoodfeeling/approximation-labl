package LinearEquationSolver;

import static java.lang.Math.abs;

public class Gaus {
    private double [][] avect;
    private double[] bvect;
    private int n;
    private double[] solution;
    private double[][] acopy;
    private double[] bcopy;
    private int counter;

    public Gaus(double [][] avect, double [] bvect, int n){
        this.avect = avect;
        this.bvect = bvect;
        this.solution = new double[n];
        this.counter = 0;
        this.n = n;
    }


    public double[] getSolution() {
        return solution;
    }

    private void replace(double[][] arr, double[] arr2, int i, int j){
        double[] temp = arr[i];
        arr[i] = arr[j];
        arr[j] = temp;
        double tmp = arr2[i];
        arr2[i] = arr2[j];
        arr2[j] = tmp;
    }

    public void makeTriangularMatrix(){
        for (int i = 0; i < n-1; i++) {
            replace(avect, bvect, i, mainchoice(i));
            double working = avect[i][i];
            if (working == 0){
                counter++;
                for (int j = i+1; j < n; j++) {
                    if(avect[j][i] != 0){
                        working = avect[j][i];
                        replace(avect, bvect,  i, j);
                        break;
                    }
                }
            }
            int l = mainchoice(i);
            if (i != l) {
                counter++;
                replace(avect, bvect, i, l);
            }
            for (int j = i+1; j < n; j++) {
                double coef = -(avect[j][i]/working);
                for (int k = 0; k < n; k++) {
                    avect[j][k] = avect[i][k] * coef + avect[j][k];


                }
                bvect[j] = bvect[i] * coef + bvect[j];
            }

        }
    }

    public void solve(){
        for (int i = n-1; i >=0 ; i--) {
            double b = bvect[i];
            for (int j = n-1; j > i; j--) {
                b -= solution[j] * avect[i][j];
            }
            solution[i] = b/avect[i][i];
        }
    }

    private int mainchoice(int a){
        int maxind = a;
        double max = avect[a][a];
        for (int i = a+1; i < n; i++) {
            if (abs(avect[i][a]) > abs(max)){
                max = avect[i][a];
                maxind = i;
            }
        }
        return maxind;
    }




}